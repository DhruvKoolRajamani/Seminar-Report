\documentclass[15pt]{report}
    \renewcommand*\familydefault{\sfdefault} 
    \usepackage{outline}
    \usepackage{pmgraph}
    \usepackage{helvet}
    \usepackage{geometry}
    \usepackage{fancyhdr}
    \usepackage[normalem]{ulem}
    \usepackage{titlesec}
    \usepackage{xcolor}
    \usepackage{amsmath}
    \usepackage{graphicx}
    \usepackage{caption}
    \usepackage{subcaption}

    \graphicspath{./images/}

    \titleformat{\chapter}[block]{\large\bfseries\filcenter}{\thechapter}{0.5em}{}
    \titleformat{\section}[block]{\bfseries}{\thesection}{0.5em}{}
    \titleformat{\subsection}[block]{\bfseries}{\thesubsection}{0.5em}{}
    
    \fancyhf{}
    \pagestyle{fancy}
    \lhead{\footnotesize{Section \thechapter}}
    \rfoot{\footnotesize{\textbf{\department}}}
    \lfoot{\footnotesize{Page \thepage}}
    \rhead{\footnotesize{\Title}}

    \setlength{\parindent}{0em}
    \setlength{\parskip}{0.5em}
    \setlength{\emergencystretch}{10pt}
    
    \renewcommand{\baselinestretch}{1.5}

    \newcommand{\Title}{Development of a controller for a flexible manipulator}
    \newcommand{\department}{Dept. of Mechatronics Engineering}
    \newcommand{\lb}{Lie-B{\"a}cklund }

    % \date{\oldstylenums{00}/\oldstylenums{00}/\oldstylenums{00}}
    %--------------------Make usable space all of page
    
    \geometry{
        a4paper,
        left=1.25in,
        top=1in,
        bottom=1in,
        right=1in
        }
    %--------------------Indention
    \setlength{\parindent}{0.5em}
    
    \begin{document}
    %--------------------Title Page
    \title{\textbf{\Title}}
    \author{
        \textbf{Author},\\ 
        \textbf{Reg No}, \\
        \textbf{4th Year}, \\
        \textbf{Section B}, \\
        \textbf{Mechatronics Engineering}
        }
    \maketitle

    %--------------------Contents
    \tableofcontents{Bibliography}
    %--------------------Begin Outline
    \chapter{Introduction}
        \begin{sloppypar}
            \noindent Manipulators are devices used in robotics to manipulate articles or materials which are located in inaccessible locations. The ISO (International Organization for Standardization) defines a manipulator as a machine in which the grasping or moving mechanism in several degrees of freedom is brought about by a series of jointed or sliding segments. A robotic manipulator may be controlled by a remote operator or a programmable electronic controller.

            \noindent In order to analyse the kinematics of the manipulator, the segments are usually considered to be rigid in nature. Each rigid segment is considered an orthonormal co-ordinate frame. The components or segments of a manipulator are connected using different types of kinematic joints, such as revolute, sliders, screw, spherical, helical or planars. Some of these joints receive input from actuators and hence are movable. Manipulators may be designed for a slow system using rigid dynamics or for a fast system using flexible link dynamics.

            \noindent  A flexible manipulator is simply a discretized set of smaller rods linked together to provide n-degrees of freedom in the joint space. Conventional control methodologies employ n actuators coupled to these n-links to fully control the motion of a flexible rod, a nonlinear mechanical system. Since such systems would be impractical, recent control methodologies have been directed at achieving under-actuation - a method of controlling the motion of system with a lower number of actuators than the degrees of freedom. Such systems are mostly nonholonomic and require a set of constraints to achieve desired motion.

            \noindent Recently, the nonholonomic mechanical systems have received much attention in the field of robotics and control engineering. Many theoretical developments have been accumulated in particular for driftless systems and prepared as powerful tools. In this report, the author presents a novel nonlinear control theory that successfully controls a flexible manipulator through a series of nonholonomic constraints.            
            \section{Nonlinear Systems}
                \noindent For any nonlinear system, we can denote the input signal by $ \dot u $ with its value at time moment $ t $ by $ u(t) $ and  $ \dot y $ is the output signal with $ y(t) $ as its value at time moment $ t $. We can assume that the signals are elements of functions spaces which are usually linear spaces having an appropriate norm and some topological properties (convergence of Cauchy sequences of functions, existence of scalar product etc.). The chosen function space (Banach space, Hilbert space etc.) may depend on the control problem to be solved. \cite{lantos_2013}

                \noindent If the state transition function satisfies some continuity conditions, then the state is the solution of the nonlinear state differential equation with appropriately chosen $ f (t, x, u) $ and the system is called continuous time smooth nonlinear system:         
                \begin{equation}
                    \begin{split}
                        \dot x = f (t, x, u), \\ y = g(t, x, u),
                    \end{split}
                \end{equation}                
                \noindent Where "dot" denote time derivative and we used short forms in notation instead of $ \dot x(t) = f (t, x(t), u(t)) $ and $ y(t) = g(t, x(t), u(t)) $. We shall consider dominantly smooth systems in this report therefore "smooth" will be omitted and the system is called continuous time nonlinear time variant system (NLTV). If $ f $ does not depend explicitly on $ t $, that is, $ f (x, y) $, the nonlinear system is time invariant (NLTI). We shall usually assume that $ x \in R^n , u \in R^m , y \in R^p $.

                \noindent The nonlinear control methodology employed in the controller is Differential Flatness. Differentially flat systems were originally studied by Fliess et al. in the context of differential algebra \cite{Fliess1} and later using Lie-Backlund transformations \cite{Fliess2}. Systems which are differentially flat have several useful properties which can be exploited to generate effective control strategies for nonlinear systems.
            \section{Holonomic and Nonholonomic Systems}
                \noindent Nonlinear systems have often to satisfy constraints that restrict the motion of the system by limiting the paths which the system can follow. For simplicity, we assume that the configuration space $ Q $ is an open subset of $ R^n $ with coordinates $ q = (q_1 , . . . , q_n )^T $ . More general configuration spaces can be handled by an appropriate choice of local coordinates.
                \subsection{Holonomic Constraints}
                    More generally, a constraint is called \textit{holonomic} if it restricts the motion of the system to a smooth hypersurface in the unconstrained configuration space Q. Holonomic constraints can be represented locally by algebraic constraints on the configuration space:
                    \begin{equation}
                        \begin{split}
                            h_i (q) = 0, \\  i = 1, . . . , k.
                        \end{split}
                    \end{equation}
                    \noindent We shall assume that the constraints are linearly independent, that is, the matrix $ \frac{\partial h}{\partial q} $ has full row rank. Since holonomic constraints define a smooth hypersurface in the configuration space, it is possible to eliminate the constraints by choosing a set of coordinates for this surface.

                    \noindent A fundamentally more \textit{general constraint} occurs in some robotics and mechanical applications (multifingered grasping, rolling disk etc.) where the constraint has the form:
                    \begin{equation}
                        A^T (q)\dot q = 0.
                    \end{equation}
                    \noindent Such a constraint is called Pfaffian constraint. Notice that $ A^T (q) $ is $ k * n $ and $ \dot q $ appears linearly in the constraint equations. This linearity gives chance to eliminate the constraints using a nonlinear coordinate transformation based on the fundamental results of differential geometry. Since Pfaffian constraints restrict the allowable velocity of the system but not necessarily the configurations, we cannot always represent it as an algebraic constraint on the configuration space. Thus, an \textit{integrable Pfaffian constraint} is equivalent to a holonomic constraint:
                    \begin{equation}
                        A^T (q)\dot q = 0 \iff \frac{\partial h}{\partial q} \dot q = 0
                    \end{equation}
                \subsection{Nonholonomic Constraints}
                    \noindent Pfaffian constraint that is not integrable is an example of \textit{nonholonomic} constraint. For such constraints, the instantaneous velocities of the system are constrained to an $ n - k $ dimensional subspace, but the set of reachable configurations is not restricted to some $ n - k $ dimensional hypersurface in the configuration space. Since not all Pfaffian constraints are integrable, hence the motion equations have to be extended to this case.

                    \noindent It is still possible to speak about constraint forces. Such forces are generated by the Pfaffian constraints to insure that the system does not move in the directions given by the rows of the constraint matrix $ A^T (q) $. The constraint forces at $ q \in Q $ are defined by $ F_c = A(q)\lambda $, where the Lagrange multiplier $ \lambda \in R^k $ is the vector of relative magnitudes of the constraint forces. The constraint forces for Pfaffian constraints prevent motion of the system in directions which would violate the constraints. In order to include these forces in the dynamics, we have to add an additional assumption, namely forces generated by the constraints do no work on the system.

                    \noindent By using the Lagrangian $ L(q, \dot q) $ for the unconstrained system and assuming the constraints in the form $ A^T (q)\dot q = 0 $ the equation of motion can be written in the form:
                    \begin{equation}
                        \frac{d}{dt} \frac{\partial L}{\partial \dot q} - \frac{\partial L}{\partial q} = A(q)\lambda + F,
                    \end{equation}
                    \noindent Where $ F $ represents the nonconservative and externally applied forces. For systems having Lagrangian $ L(q, \dot q) = \frac{1}{2} \dot{q^T} M(q)\dot q - P (q) $ (kinetic energy minus potential energy), the motion equations can be written as:
                    \begin{equation}
                        M(q) \ddot q + C(q, \dot q)\dot q + n(q, \dot q) = A(q)\lambda + F,
                    \end{equation}
                    Where $ n(q, \dot q) $ denotes nonconservative and potential forces and $F$ is the external force.
                \section{Differential Flatness}
                    \noindent The idea of differentially flat systems arose at the 90th years based on the works of David Hilbert and Elie Cartan at the beginning of the 20th century.
                    
                    \noindent It remains a serious problem to obtain the symbolic solution of a system of partial differential equations (Frobenius equations) for which no general and efficient methods are available. All these offer reason for seeking special solutions in which range the flatness control is a possible approach.

                    \noindent A system $ \dot x = f(x, u) $ with state vector $ x \in R^n $, input vector $ u \in R^m $ where $ f $ is a smooth vector field, is differentially flat if there exists a vector $ y \in R^m $ in the form\cite{murray_cat}:
                    \begin{equation}
                        y = h(x, u, \dot u , . . . . . . , u^r ),
                    \end{equation}
                    \noindent Such that:
                    \begin{equation}
                        \begin{split}
                            x = \phi(y, \dot y , . . . . . . , y^p ), \\
                            u = \alpha(y, \dot y , . . . . . . , y^q ),
                        \end{split}                       
                    \end{equation}
                    \noindent Where $ h, \phi $, and $ \alpha $ are smooth functions. Then, $ y $ is a system with flat outputs $ z_1, z_2, ..... , z_q $.

                    \noindent For a differentially flat system, all of the feasible trajectories for the system can be written as functions of a flat output $ y $ and its derivatives. The number of flat outputs is always equal to the number of system inputs.
                    \begin{figure}[h]
                        \includegraphics[scale=0.5]{./images/flatness1}
                        \centering
                        \caption{Examples of systems with Flat Outputs. \cite{murray_cat}}
                    \end{figure}
                    \subsection{Huygen's Center of Oscillation}
                        Point in a physical pendulum, on the line through the point of suspension and the centre of mass, which moves as if all the mass of the pendulum were concentrated there. In other words, the point at which all impulsive reaction forces cancel out.
                        \begin{figure}[h]
                            \includegraphics[scale=0.3]{./images/coo}
                            \centering
                            \caption{The inverted pendulum in the vertical plane.}
                        \end{figure}
                        \noindent For an inverted pendulum, the Huygens oscillation centre is the linearizing output or to say flat output. Hence all the system parameters can be expressed in terms of its coordinates and their derivative.
                    \subsection{\lb Equivalence}
                        \noindent Two systems are said \lb   equivalent if and only if there exists a smooth one-to-one time-preserving mapping between their integral curves (trajectories that are solutions of the system differential equations) which maps tangent vectors to tangent vectors, to preserve time differentiation.

                        \noindent A flat system is \lb  equivalent to a system whose integral curves have no differential constraints (ordinary curves), that we call trivial system. Thus, finally, a system is flat if and only if it is Lie-Backlund equivalent to a trivial system.

                        \noindent Transformations satisfying the following three properties are called \lb  isomorphisms:
                        \begin{itemize}
                            \item Every component of the transformation only depends on a finite, but otherwise not known in advance, number of successive time derivatives of the coordinates,
                            \item The transformations preserve differentiation with respect to time,
                            \item They are "invertible", in the sense that one can go back to the original coordinates by transformations of the same type.
                        \end{itemize}
                    \subsection{Systems in Triangular Form}
                        \noindent The state-space description of systems in triangular form is as follows:
                        \begin{gather}
                            \dot x_1 = f_1 (x_1 ) + g_1 (x_1 )x_2 \\
                            \dot x_2 = f_2 (x_1 , x_2 ) + g_2 (x_1 , x_2 )x_3 \\
                            \dot x_3 = f_3 (x_1 , x_2 , x_3 ) + g_3 (x_1 , x_2 , x_3 )x_4 \\
                            .............................. \\
                            \dot x_i = f_i (x_1 , x_2 ... ..., x_i ) + g_i (x_1 , x_2 ... ..., x_i )x_{i+1} \\
                            .............................. \\
                            \dot x_n = f_n (x_1 , x_2 ... ..., x_n ) + g_n (x_1 , x_2 ... ..., x_n )u
                        \end{gather}
                        \noindent The flat output for such systems is $ y = x_1 $.
                \section{Controllability}
                    The scalar pair $(A, B)$ is controllable if, given a duration $ T > 0 $ and two arbitrary points $ x_0 , x_T \in R_n $, there exists a piecewise continuous function $ t \rightarrow \overline{u}(t) $ from $ [0, T] $ to $ R^m $, such that the integral curve $ \overline{x}(t) $ generated by $ \overline{u} $ with $ \overline{x}(0) = x_0 $, satisfies $ \overline{x}(t) = x_T $.
                    \subsection{Kalman's Theorem}
                        \noindent A necessary and sufficient condition for $ (A, B) $ to be controllable is:
                        \begin{equation}
                            rank(C) = rank(B|AB|...|A^{N-1}B) = n
                        \end{equation}
                        \noindent Where $C_{n*nm}$ is the Controllability matrix and $n$ is the length of $A$.
        \end{sloppypar}
    
    \chapter{Literature Review}
        The author of this report has conducted a thourough literature review of various papers that are closely related to the current project. The author has summarized the reviews incisively.
        \begin{sloppypar}
            \section{A Test for Differential Flatness by Reduction to Single Input Systems\cite{paper1}}
                \noindent Two planar rigid links are hinged at a point. The system contains two body-fixed forces $f_1$ and $f_2$ which act on the first link at point P. The point P lies on the line joining the point O and $G_1$, the centre of mass of the first body. The third input is a pure torque, $\tau$, between the two bodies.
                \begin{figure}[h]
                    \includegraphics[scale=0.35]{./images/link2}
                    \centering
                    \caption{Two coupled rigid bodies.}                    
                \end{figure}

                \noindent For 3 inputs there should be minimum 3 flat outputs. First, the 2 outputs parameters are guessed, and the system is tested for flatness. The two flat outputs are given by coordinates of a body fixed point in the second body, i.e $(x_1, y_1 )$ such that $(\lambda_1 , \lambda_2 )$ are its coordinates in the body fixed frame. The third output is then formulated in such a way that all the system’s state and input forces are just the function of these three outputs and their derivatives.

                \noindent In this paper, the authors presented a method for testing flatness of a $p$ input system by reducing to the case of a single input. This method requires making an educated guess for $p-1$ of the flat outputs.
            \section{Trajectory generation for a towed cable system using differential flatness\cite{paper2}}
                \noindent The equations of motion for the given towed cable system are formulated by the finite element method. The segments of the cable are discretized into by rigid links and the forces acting on each segment are lumped and applied at the end of each rigid link. 
                
                \noindent It is shown in the paper that this system is differentially flat, even in the presence of aerodynamic drag and known wind, with the position of the bottom of the cable as the differentially flat output. The trajectory generation problem reduces to searching for trajectories of the drogue which give an appropriately bounded motion for the tow plane.
                \begin{figure}[h]
                    \includegraphics[scale=0.4]{./images/cable}
                    \centering
                    \caption{Relative equilibrium of a cable towed in a circle.}
                \end{figure}
            \section{Differential flatness of mechanical control system: A catalogue of the prototype system.\cite{murray_cat}}
                \noindent Differentially flat systems have useful properties which can be exploited to generate effective control strategies for nonlinear systems.
                \newpage
                \begin{figure}[h]
                    \includegraphics[scale=0.4]{./images/difftable}
                    \centering
                    \caption{Flatness of a rigid body with body fixed forces and torques.}
                \end{figure}

                \noindent The authors concentrate on characterizing differential flatness for mechanical control systems. More specifcally, the authors attempt to exploit the structure of second order systems whose unforced motion is described by Lagrangian mechanics. There are several existing results which indicate that
                differential flatness for this class of systems is highly dependent on the special structure available due to the Lagrangian nature of the system.
                \begin{figure}[h]
                    \includegraphics[scale=0.4]{./images/chain3}
                    \centering
                    \caption{A chain of coupled rigid bodies in the plane.}
                \end{figure}

                \noindent The chain of rigid bodies is couple with forces acting at the connections between successive links. The interconnected hinged points on each rigid body and the centre of mass of the link lie on the same line. If the centre of oscillation of the last rigid body in the chain is chosen as an output (flat output), then the motion of the chain, as well as the forces applied at the point of contact, can be expressed in terms of the flat output and its finite derivative.
            \section{Differentially flat design of under-actuated planar robots: Experimental results.\cite{paper4}}
                \noindent Differential flatness provides a systematic approach to plan and control feasible trajectories for the systems. The author has formulated a philosophy to design differentially flat underactuated planar manipulators. The design philosophy has two sufficient conditions: 
                \begin{itemize}
                    \item An inertia distribution scheme
                    \item An actuator and torque spring placement scheme
                \end{itemize}
                \begin{figure}[h]
                    \centering
                    \begin{subfigure}{.5\textwidth}
                      \centering
                      \includegraphics[width=.7\linewidth]{./images/armreal}
                      \caption{The prototype 3 link manipulator}
                      \label{fig:sub1}
                    \end{subfigure}%
                    \begin{subfigure}{.5\textwidth}
                      \centering
                      \includegraphics[width=.7\linewidth]{./images/armline}
                      \caption{Line diagram}
                      \label{fig:sub2}
                    \end{subfigure}
                    \caption{Flattened 3-DOF Robotic Arm}
                    \label{fig:arm}
                \end{figure}
                \noindent The given system is an underactuated system as there are only two actuators present. One is at the first joint and the other is at the second joint. The mass of the links are distributed such that the COM of link 3 lies on the 3rd joint and COM of the 2nd link lies on joint 2. The third joint is not actuated and is passive, with a torsional spring attached to it.
        \end{sloppypar}
    \chapter{Methodology}
        \begin{sloppypar}
            \section{Objective}
                \begin{figure}[h]
                    \includegraphics[scale=0.3]{./images/robarm}
                    \centering
                    \caption{Conventional robotic end effector control.}
                \end{figure}
                \noindent Robotic manipulators are required to perform highly repeatable, accurate and versatile tasks. To achieve the same, conventional manipulators inculcate multiple actuators to actuate various redundant joints.

                \noindent Thus, the objective is to implement a trajectory based control of the end point of a n-link flexible manipulator using minimal input actuators.
            \section{Control Algorithm}
                \begin{enumerate}
                    \item Consider a 3-link system, fixed at the pivot of the first link, with each individual link acting as a differentially flattened pendulum. The differentially flat output for each individual link can be found to be the Huygen’s Center of Oscillation $ (r + \frac{J}{Mr}) $.
                    \item Now, consider the output of each link to be the center of oscillation which is coupled to the pivot point of the next link. The dynamics of this system can now be recursively derived for each individual link, with the input as the output of the previous link.
                    \item Simplify the system dynamics to achieve input parameters at the first link in terms of flat outputs of the 3rd link.
                    \item Model the system dynamics on MATLAB and using ODE45 or SIMULINK, derive a cubic spline by supplying a set of initial and final conditions of the states to the system. This yields the input forces/torques \footnote{The current system has been modelled for a controlled torque input to the first link. Similarly, another system can be modelled using a controlled
                    force input to the first link with the first link free to move and not fixed, similar to an inverted pendulum on a cart.} required for the 1st link required for the selected cubic trajectory.
                    \item Torsional springs can now be added to each joint to provide effective dampening and help achieve a more controlled motion.
                    \item Re-formulate the dynamics of the system and repeat steps 2-4.
                    \item The system can now be successively modelled for n-links.
                \end{enumerate}
            \section{Modelling}
                \noindent A 3-link inverted pendulum fixed at the base is discretized into three pendulums having the input at the suspension point. A general $i^{th}$ link treated as a pendulum having some forces at the end from $(i+1)^{th}$ link and inputs motion and force at another end (by the $(i-1)^{th}$ link).
                \begin{figure}[h]
                    \centering
                    \begin{subfigure}{.5\textwidth}
                      \centering
                      \includegraphics[width=.5\linewidth]{./images/force}
                      \caption{Force Diagram}
                      \label{fig:sub3}
                    \end{subfigure}%
                    \begin{subfigure}{.5\textwidth}
                      \centering
                      \includegraphics[width=.5\linewidth]{./images/accel}
                      \caption{Acceleration diagram}
                      \label{fig:sub4}
                    \end{subfigure}
                    \caption{Dynamics and Kinematics of a single link}
                    \label{fig:link}
                \end{figure}
                \begin{figure}[ht]
                    \includegraphics[scale=1]{./images/link3planar}
                    \centering
                    \caption{Three uniform link system with two input forces.}
                \end{figure}

                \begin{equation}
                    M \ddot X_{CM} = F_{X_{i}} - F_{X_{i+1}}
                \end{equation}
                \begin{equation}
                    M \ddot Z_{CM} = F_{Z_{i}} - F_{Z_{i+1}} - Mg
                \end{equation}
                \begin{equation}
                    \frac{J}{d} \ddot \theta_i = (F_{X_{i}} + F_{X_{i+1}})sin\theta_i - (F_{Z_{i}} + F_{Z_{i+1}})cos\theta_i
                \end{equation}

                \noindent Let,
                \begin{equation*}
                    \begin{split}
                        x=\frac{X_{CM}}{g}; z=\frac{Z_{CM}}{g}; R_X=\frac{F_X}{Mg}\\
                        R_{X_{i}} = \frac{F_{X_{i}}}{Mg};R_{Z_{i}} = \frac{F_{Z_{i}}}{Mg};
                        \epsilon_i=\frac{J}{Mgd} \\ 
                        u_{1_{i}} = \frac{F_{X_{i}} - R_{X_{i}}}{Mg}; u_{2_{i}} = \frac{F_{Z_{i}} - R_{Z_{i}}}{Mg} - 1; 
                    \end{split}
                \end{equation*}
                \noindent Where $x$ and $z$ are normalised coordinate of the center of mass of the $i^{th}$ link, $\epsilon_i$ is the distance of hugyen oscillation center from the center of mass of the $i^{th}$ link , $R_{X_{i}}$ and $R_{Z_{i}}$ are the normalized forces acting on the $i^{th}$ link due to the $(i+1)^{th}$ link.
                \begin{equation}
                    \epsilon_i \ddot \theta_i = -(u_{1_{i}} + R_{X_{i}})cos \theta_i - (u_{2_{i}} + 1 + R_{Z_{i}})sin \theta_i 
                \end{equation}
                \noindent Also $\xi$ and $\zeta$ are the coordinates of the COM of the $i^{th}$ link, $\epsilon_i$ is the distance of huygen center of oscillation from the COM of the $i^{th}$ link, $R_{X_{i}}$ and $R_{Z_{i}}$ are the normalized forces acting on the $i^{th}$ link due to the $(i+1)^{th}$ link.
                \begin{equation}
                    \theta_i = tan^{-1} \frac{(\ddot \xi + 2R_{X_{i}})}{(\ddot \zeta + 1 + 2R_{Z_{i}})}
                \end{equation}
                \begin{equation}
                    X_i = \xi - (\epsilon + d_i)sin \theta_i
                \end{equation}
                \begin{equation}
                    Z_i = \tau - (\epsilon + d_i)cos \theta_i
                \end{equation}
                \noindent The uniform link assembly without any torsional spring in between with two input forces at the end of the first link is flat if the attachment points of the links are at Huygen centre of
                oscillation of the previous link.
        \end{sloppypar}
    
    \chapter{Results and Conclusion}
        \noindent Since the system turned out to be singular when each link had equal length, an innovative mass distribution was applied with every links mass to be equal to the sum of the masses of all successive links. Assuming equal density and thickness for each link, the length of each link could be varied to achieve the above mass distribution. The link lengths were assumed to be 0.2m, 0.1m, 0.1m.

        \begin{figure}[h]
            \includegraphics[scale=0.6]{./images/r1}
            \centering
            \caption{X-Coordinates location during the trajectory}
        \end{figure}
        \begin{figure}[h]
            \includegraphics[scale=0.6]{./images/r2}
            \centering
            \caption{Y-Coordinates location during the trajectory}
        \end{figure}

        \noindent A n-link system is differentially flat if the input to the successive link lies on the current links center of oscillation. The trajectory of such a system can be controlled by supplying an input force to the first link.
    

    \begin{thebibliography}{1}
      
        \bibitem{lantos_2013} Lantos, Bla. {\em Nonlinear Control of Vehicles and Robots} 2013: Springer
        
        \bibitem{Fliess1}  M. Fliess, J. Levine, P. Martin, and P. Rouchon {\em On
        differentially flat nonlinear systems} 1992: Comptes Rendus des Seances de l'Academie des Sciences, Serie 1
      
        \bibitem{Fliess2} M. Fliess, J. Levine, P. Martin, and P. Rouchon {\em Linearisation par bouclage dynamique et transformations de Lie-Backlund} 1992: Comptes Rendus des Seances de l'Academie des Sciences, Serie 1
      
        \bibitem{murray_cat} R. M. Murray, M. Rathinam and W. Sluis, {\em Differential Flatness of Mechanical Control Systems-A Catalog of Prototype Systems}, 1995: ASME international mechanical engineering congress and exposition., San Francisco.

        \bibitem{paper1} M. Rathinam and W. M. Sluis, {\em A test for differential flatness by reduction to single input systems}, 1996: IFAC Proceedings.

        \bibitem{paper2} R. M. Murray, {\em Trajectory generation for a towed cable system using differential flatness.}, 1996: IFAC world congress., San Francisco.

        \bibitem{paper4} V. Sangwan, H. Kuebler and S. K. Agrawal, {\em Differentially flat design of under-actuated planar robots: Experimental results}, 2008: IEEE International Conference on Robotics and
        Automation, Pasadena, CA, USA.
      
    \end{thebibliography}

    \end{document}